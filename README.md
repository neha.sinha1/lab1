# K8s_Lab_08 | ConfigMap

In this lab, we will try to understand the concepts and working of  ConfigMap
in Kubernetes
## Pre-requisites
- Deployment

## Steps

In this lab, we are going to understand the concept of ConfigMap:-
- Deploy the Gateway application inside Kubernetes.
- Externalize the configuration of gateway configuration and mount it to gateway pod.
- Create another configmap with key-value data and mount it to the attendance application.

## Headless
### Create the ConfigMap for gateway application

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: gateway-config
data:
  application.yml: |
    spring:
      profiles: default

    eureka:
    client:
      healthcheck:
        enabled: true

    zuul:
    debug:
      request: true

    routes:
      employee:
      path: /employee/**
      url: http://empms-employee:8083/employee
      service-id: /employee/**

      attendance:
      path: /attendance/**
      url: http://empms-attendance:8081/attendance
      service-id: /attendance/**

      salary:
      path: /salary/**
      url: http://empms-salary:8082/salary
      service-id: /salary/**
```
Save the above manifest as gateway-configmap.yaml

Now create the configmap of gateway application by following command
```
kubectl apply -f gateway-configmap.yaml
```
To check the created configmap execute the below command
```
kubectl get configmap
```
Output :
```
NAME                  DATA   
attendence-configmap   5   
```


### Create the deployment for gateway application and mount the configmap to it.
```yaml

---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: empms-gateway
  name: empms-gateway
spec:
  replicas: 1
  selector:
    matchLabels:
      app: empms-gateway
  template:
    metadata:
      labels:
        app: empms-gateway
    spec:
      containers:
      - image: opstree/empms-gateway:1.0
        imagePullPolicy: Always
        name: empms-gateway
        ports:
        - containerPort: 8080
        volumeMounts:
        - mountPath: /app/config/application.yml
          name: gateway-config
          subPath: application.yml
      volumes:
        - name: gateway-config
          configMap:
            name: gateway-config
```
Save the above manifest as gateway-deployment.yaml

Now create the deployment of gateway application by following command
```
kubectl apply -f gateway-deployment.yaml
```
To check deployment of gateway application execute the below command
```
kubectl get pod
```
Output :
```
NAME                                READY   STATUS    RESTARTS 
empms-gateway-799486fdd7-2wdks      1/1     Running      0

```
### Access the gateway pod to  check the mounted configmap
```
 kubectl exec -it empms-gateway-799486fdd7-2wdks -- sh

```
Now go to the directory,where you mounted the configmap

```
 cd /app/config/
 cat application.yml 
```
It will show the content of configuration that we had mentioned in configmap.

### Create  another configmap with key-value data and mount it to the attendance applicatiion
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: attendence-configmap
data:
  # property-like keys; each key maps to a simple value
  MYSQL_USERNAME: "root"
  MYSQL_PASSWORD: "password"
  MYSQL_HOST: "empms-db"
  MYSQL_DATABASE: "attendancedb"
  SLEEP_INTERVAL: "30"
  ```
Save the above manifest as attendance-configmap.yaml

Now create the configmap of attendence application by following command
```
kubectl apply -f attendance-configmap.yaml
```
To check the created configmap execute the below command
```
kubectl get configmap
```
Output :
```
NAME                   DATA   
attendence-configmap   5      
gateway-config         1       
```
As attendence application has dependency on mysql,first create and apply it's deployment and service .
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: empms-db
  name: empms-db
spec:
  replicas: 1
  selector:
    matchLabels:
      app: empms-db
  template:
    metadata:
      labels:
        app: empms-db
    spec:
      containers:
      - image: opstree/empms-db:1.0
        imagePullPolicy: Always
        name: empms-db
        env:
        - name: MYSQL_DATABASE
          value: attendancedb
        ports:
        - containerPort: 3306

```
Save the above manifest file as mysql_deploy.yaml
Now create the deployment  of mysql application by following command
```
kubectl apply -f mysql_deploy.yaml
```




Service file of mysql application

```yaml

kind: Service
apiVersion: v1
metadata:
  name: empms-db
spec:
  type: ClusterIP
  selector:
    app: empms-db
  ports:
  - protocol: TCP
    port: 3306
```
Save the above maifest file as mysql_service.yaml and create the service  of mysql application by following command
```
kubectl apply -f mysql_service.yaml
```
In attendence deployment ,pair the  key-value of congfigmap with environments variables of mysql application  and also mount the same configmap into it .
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: empms-attendance
  name: empms-attendance
spec:
  replicas: 1
  selector:
    matchLabels:
      app: empms-attendance
  template:
    metadata:
      labels:
        app: empms-attendance
    spec:
      initContainers:
      - name: init-mysql
        image: opstree/empms-mysql-healthcheck:1.0
        imagePullPolicy: Always
        env:
          - name: MYSQL_USERNAME                                      
            valueFrom:
              configMapKeyRef:
                name: attendence-configmap          # The ConfigMap this value comes from.
                key: MYSQL_USERNAME                  # The key to fetch.
          - name:  MYSQL_PASSWORD
            valueFrom:
              configMapKeyRef:
                name: attendence-configmap
                key:  MYSQL_PASSWORD
          - name:  MYSQL_HOST
            valueFrom:
              configMapKeyRef:
                name: attendence-configmap
                key:   MYSQL_HOST  
          - name: MYSQL_DATABASE
            valueFrom:
              configMapKeyRef:
                name: attendence-configmap
                key:  MYSQL_DATABASE
          - name:  SLEEP_INTERVAL
            valueFrom:
              configMapKeyRef:
                name: attendence-configmap
                key:  SLEEP_INTERVAL
      containers:
      - image: opstree/empms-attendance:2.0
        imagePullPolicy: Always
        name: empms-attendance
        ports:
        - containerPort: 8081
        volumeMounts:
        - name: attendence-configmap
          mountPath: "/config"      
    
        
      volumes:
        - name: attendence-configmap
          configMap:
            name: attendence-configmap
 ```    
Save the above maifest file as attendence_deploy.yaml and create the deployment  of attendence  application by execute following command
```
kubectl apply -f attendance_deploy.yaml
```
### Check the pods
Execute the below command.
```
kubectl get pod
```
Output:
```
NAME                                READY   STATUS    RESTARTS       
empms-attendance-7c7d85469b-xz72r   1/1     Running   0              
empms-db-5478fb68b7-gv75r           1/1     Running   0              
empms-gateway-799486fdd7-2wdks      1/1     Running   0   
```
Now access attendence application pod 
```
kubectl exec -it empms-attendance-7c7d85469b-xz72r  -- sh 
```

Curl mysql application to check the connection by execute below command 
```
curl localhost:8081/attendance/healthz
```
Output:
```
/app # curl localhost:8081/attendance/healthz
{"database":"MySQL","message":"MySQL is running","status":"up"}/app # 

```
You can go to the configmap mounted directory and list all key-pair of configmap .
```
cd /config
ls -l

```
Output:
```
/config # ls -l
total 0
lrwxrwxrwx    1 root     root            21 Mar 30 17:08 MYSQL_DATABASE -> ..data/MYSQL_DATABASE
lrwxrwxrwx    1 root     root            17 Mar 30 17:08 MYSQL_HOST -> ..data/MYSQL_HOST
lrwxrwxrwx    1 root     root            21 Mar 30 17:08 MYSQL_PASSWORD -> ..data/MYSQL_PASSWORD
lrwxrwxrwx    1 root     root            21 Mar 30 17:08 MYSQL_USERNAME -> ..data/MYSQL_USERNAME
lrwxrwxrwx    1 root     root            21 Mar 30 17:08 SLEEP_INTERVAL -> ..data/SLEEP_INTERVAL

```
